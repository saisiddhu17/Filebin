require('dotenv').config()
const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');
const AWS = require('aws-sdk');
const fs = require('fs');
const conection = require('./db')
const { v4: uuidv4 } = require('uuid');
const ObjectID = require('mongodb').ObjectID;



const BUCKET_NAME = "filebin-files";
const IAM_USER_KEY = process.env.KEY || "";
const IAM_USER_SECRET = process.env.SECRET || "";
const s3bucket = new AWS.S3({
  accessKeyId: IAM_USER_KEY,
  secretAccessKey: IAM_USER_SECRET
});

const app = express();
const port = process.env.PORT || 3000;

app.set('view engine', 'ejs')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'));

app.use(fileUpload({
  createParentPath: true,
  limits: { fileSize: 20 * 1048576 } // 20 Mb 
}));

app.get('/', (req, res) => {
  res.render("home")
})

app.post('/upload', async (req, res) => {

  console.log(req.files);
  if(req.files) {
    const params = {
      Bucket: BUCKET_NAME,
      Key:  req.files.doc.name,
      Body: req.files.doc.data
    };
  
    try {
      let s3Response = await s3bucket.upload(params).promise();
      console.log(s3Response);
  
      const db = await conection.getClient("mongodb");
      const collection = db.collection('files');
  
      let doc = {
        filename: req.files.doc.name,
        downloadCount: 0,
        active: true,
        s3Response,
        insertedAt: new Date(),
        session: uuidv4()
      }
  
      let dbResponse = await collection.insert(doc, {w: 1})
      console.log(dbResponse)
      res.redirect(`/file/${dbResponse.ops[0].session}`)
      // res.send({"code": 0, "data": dbResponse.ops[0].session})
  
      
    } catch (e) {
      console.error(e);
      res.status(500).send({code: -1, data: e})
    }
  } else {
    res.status(400).send({code: -1, data: "Bad Request, No file given"})
  }
  


});

app.get("/file/:session", async (req, res) => {
  let db = await conection.getClient("mongodb");
  const collection = db.collection('files');
  let files = await collection.find({session: req.params.session, downloadCount: {$lt: 1}}).toArray()
  // Insert some documents
  res.render('files', {files})
  // res.send(files)
});

app.get("/download/:fileId", async (req, res) => {
  let db = await conection.getClient("mongodb");
  const collection = db.collection('files');
  console.log("ID: ", req.params.fileId)
  let fileDetails = await collection.findOne({_id: ObjectID(req.params.fileId), downloadCount: {$lt: 1}})

  if(fileDetails) {

    // TODO remove the file either delete it or softdelete it
    // collection.deleteOne({_id: ObjectID(req.params.fileId)}).then(response => console.log("Deleted Successfully", response))
    collection.updateOne({_id: ObjectID(req.params.fileId)}, {$set: {downloadCount: fileDetails.downloadCount+1, active: false}}).then(response => console.log("Deleted Successfully"))

    const params = {
      Bucket: BUCKET_NAME,
      Key: fileDetails.filename
    }

    res.set('Content-disposition', 'attachment; filename=' + fileDetails.filename);
    s3bucket.getObject(params)
    .createReadStream()
      .on('error', function(err){
        res.status(500).json({error:"Error -> " + err});
    }).pipe(res);
  } else {
    // res.status(404).send({code: -1, data: "File not found"})
    res.render('error');
  }
  
  
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})



