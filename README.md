# Filebin

Filebin is used to share files temporarly with any user

## Installation
Use the node package manager npm to run in local.

```bash
npm install
```

## Usage
Create a .env file with the following information or just rename ```.env.example``` file
```env
MONGO_URL=<mongodb_url>
KEY=<aws access key>
SECRET=<aws secret>
```

## Deployment

```bash
node index.js
```
This will start the server in port 3000

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)