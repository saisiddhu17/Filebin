const MongoClient = require('mongodb').MongoClient;
const url = process.env.MONGO_URL || '';
const dbName = 'filebin';
let client = null;


const getClient = async (dbType) => {

    if(dbType === "mongodb") {
        if(client) {
            return client.db("filebin");
        } else {
            console.log("connecting")
            try {
                client = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
            } catch (e) {
                console.log(e);
            }
            return client.db("filebin");
        }
    } else {
        return client;
    }
  }

  exports.getClient = getClient